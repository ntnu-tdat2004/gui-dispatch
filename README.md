# GUI example where a function is dispatched to the GUI event loop

## Prerequisites
* Linux or MacOS
* The C++ IDE [juCi++](https://gitlab.com/cppit/jucipp) recommended

## Installing dependencies
The needed dependencies are already installed if you have installed juCi++.

### Debian based distributions
`sudo apt-get install libgtkmm-3.0-dev`

### Arch Linux based distributions
`sudo pacman -S gtkmm3`

### MacOS
`brew install gtkmm3`

## Compile and run
```sh
git clone https://gitlab.com/ntnu-tdat2004/gui-dispatch
juci gui-dispatch
```

Choose Compile and Run in the Project menu.
