#include "dispatcher.hpp"
#include <gtkmm.h>
#include <iostream>
#include <thread>

using namespace std;

class Window : public Gtk::Window {
public:
  Gtk::Box vbox;
  Gtk::TextView text_view;
  Gtk::Button button;
  Dispatcher dispatcher;

  Window() : vbox(Gtk::Orientation::ORIENTATION_VERTICAL) {
    set_default_size(400, 400);
    button.set_label("Click here");

    vbox.pack_start(text_view);
    vbox.pack_end(button, Gtk::PackOptions::PACK_SHRINK);

    add(vbox);
    show_all();

    button.signal_clicked().connect([this] {
      // Perform simulated time-consuming work in a separate thread
      thread a_thread([this] {
        for (int c = 0; c < 10; c++) {
          this_thread::sleep_for(200ms); // Simulate time-consuming work
          // dispatch potential results to GUI event loop
          dispatcher.post([this, c] {
            text_view.get_buffer()->insert(
                text_view.get_buffer()->get_insert()->get_iter(),
                to_string(c));
          });
        }
      });
      a_thread.detach(); // Let thread run separately from thread object
    });
  }
};

int main() {
  Gtk::Main gtk_main;
  Window window;
  gtk_main.run(window);
}
